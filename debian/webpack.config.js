var webpack = require('webpack');

module.exports = {
	entry: {
		'ssm': './src/ssm.js'
	},
	output: {
		library: 'ssm',
		libraryTarget: 'umd',
		libraryExport: 'default',
		filename: 'dist/ssm.min.js'
	},
	devtool: 'source-map',
	externals: {
		'ssm': 'window.ssm'
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({sourceMap:true})
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: '/usr/lib/nodejs/babel-loader',
				options: {
					presets: [['es2015', {modules: false}]]
				}
			}
		]
	}
};
